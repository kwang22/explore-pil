# Kaiyu Wang
# day 2 2020\10\13
# today I learned how to filter my picture.

# Import PIL modules
from PIL import Image, ImageFilter 

# Import sys module for sys.argv(to get filename)
import sys
# Get filename from argument
filename = sys.argv[1] 

# Open the image file
image = Image.open(filename) 

# ===================================================
# Add effect to filter. I write it by my own.
image = image.filter(ImageFilter.CONTOUR) 
# ===================================================

# Save the output image
image.save('output2.png')
