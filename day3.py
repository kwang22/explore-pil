# Kaiyu Wang
# day 3 2020\10\14
# In day 2,3,4, I learned different way to filter my picture.



# Import PIL modules
from PIL import Image, ImageFilter 

# Import sys module for sys.argv(to get filename)
import sys
# Get filename from argument
filename = sys.argv[1] 

# Open the image file
image = Image.open(filename) 

# ===================================================
# Add effect to filter
image = image.filter(ImageFilter.EMBOSS) 
# ===================================================

# Save the output image
image.save('output3.png')
